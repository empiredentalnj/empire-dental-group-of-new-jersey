We are a cosmetic dentist that offers services like checkups, teeth cleaning, crowns, implants, veneers and more.
We also perform emergency dentist services that include bonding, root canals, crowns, and bridges.

Address: 2515 CR 516, Old Bridge, NJ 08857, USA

Phone: 732-607-0909

Website: http://empiredentalnj.com
